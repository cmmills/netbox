#! /usr/bin/env python3

from telnetlib import STATUS
from unicodedata import name
import pynetbox

token = '0123456789abcdef0123456789abcdef01234567'
url = 'http://10.0.0.134:8000'

nb = pynetbox.api(url, token=token)

cluster_type = nb.virtualization.cluster_types.create(
    name="proxmox",
    slug="proxmox",
    description="Proxmox cluster",
)

cluster = nb.virtualization.clusters.create(
    name='Proxmox-dev',
    slug='proxmox-dev',
    description='Cluster 1',
    status="active",
    tenant=1,
    site=1,
    type=cluster_type.id,
)
