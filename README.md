# Netbox environment build

## Setup
Run the following scripts in the following order
- setup_base.py
- create_platform.py
- devices.py
- cluster.py
    - There doesn't seem to be a way to programmatically add devices to a cluster this needs to be done manually.
- wireless.py

## Backup postgresql database

```docker exec -i netbox-docker_postgres_1 /usr/local/bin/pg_dump -U netbox >> postgres-backup.sql```
This will connect to the running postgresql database and backup the database
- the database is housed under the postgres user within a data directory.
```bash
bash-5.1# su - postgres
a70c126867de:~$ ls -al
total 20
drwxr-xr-x    1 postgres postgres      4096 Oct 30 19:16 .
drwxr-xr-x    1 root     root          4096 Oct  7 01:17 ..
-rw-------    1 postgres postgres       166 Oct 30 19:19 .ash_history
-rw-------    1 postgres postgres        13 Oct 30 19:16 .psql_history
drwx------   19 postgres postgres      4096 Oct 30 15:41 data
```

### Docs
- [Netbox link](https://github.com/netbox-community/netbox)