#! /usr/bin/env python3

from unicodedata import decimal
import pynetbox

token = "0123456789abcdef0123456789abcdef01234567"
url = "http://10.0.0.134:8000"
nb = pynetbox.api(url, token=token)

wifi_group = nb.wireless.wireless_lan_groups.create(
    name="Home wifi",
    slug="home-wifi",
    description="Home wifi",
)

wifi_lan = nb.wireless.wireless_lans.create(
    ssid="Blackknight",
    description="Millsresidence wifi",
    auth_psk="password",
    tenant=1,
    site=1,
    group=1,        # This is the group we created above, with the id of 1
)
