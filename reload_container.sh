#!/usr/bin/env bash

clear
echo "Stopping container"
docker-compose down
echo "removing containers"
docker rm $(docker ps -a -q)
echo "removing volumes"
docker volume rm $(docker volume ls -q)
echo "starting container";echo
docker-compose up -d
sleep 3 && docker logs -f netbox-docker_netbox_1