#! /usr/bin/env python

import pynetbox

token = "0123456789abcdef0123456789abcdef01234567"
url = "http://10.0.0.134:8000"

country_region_name = "Canada"
country_region_slug = "CA"
country_region_desc = "Canada"

country_region_section_name = "Ontario"
country_region_section_slug = "ON"
country_region_section_desc = "Ontario"

local_region = "Toronto"
local_region_slug = "TOR"
local_region_desc = "Toronto Region"

tenant = "Mills_Hosting"
tenant_desc = "Home Network"

tenant_group_1 = "Home_Network"
tenant_slug_1 = "home_network"
tenant_group_desc_1 = "Home Networks"

site_group_1_name = "Mills_Hosting"
site_group_1_slug = "Mills-Hosting"

# Site number one
site_name = "Mills_site"
site_slug = "mills-site"
site_desc = "Home Network"
site_time_zone = "America/Toronto"
site_physical_address = "25 Turntable Rd"
site_shipping_address = "Same as physical address"
site_facility = "third_room"
site_status = "planned"

location_1 = "millresidence"
location_desc_1 = "Mills Network"
location_address_1 = "25 Turntable Rd"
location_slug_1 = "millresidence"

# Create a NetBox object no cert verification
nb = pynetbox.api(url, token=token)

# Create site and dependencies based on http://netbox_ip:8000/static/docs/getting-started/planning/#tenancy
country_region = nb.dcim.regions.create(
    name=country_region_name,
    slug=country_region_slug,
    description=country_region_desc,
    parent=None)

country_region_section = nb.dcim.regions.create(
    name=country_region_section_name,
    slug=country_region_section_slug,
    description=country_region_section_desc,
    parent=country_region.id)

local_region = nb.dcim.regions.create(
    name=local_region,
    slug=local_region_slug,
    description=local_region_desc,
    parent=country_region_section.id)

tenant_group_1 = nb.tenancy.tenant_groups.create(
    name=tenant_group_1,
    slug=tenant_slug_1,
    description=tenant_group_desc_1)

tenant = nb.tenancy.tenants.create(
    name=tenant,
    slug=tenant,
    parent=country_region.id,
    group=tenant_group_1.id,
    description=tenant_desc)

site_group_1 = nb.dcim.site_groups.create(
    name=site_group_1_name,
    slug=site_group_1_slug,
    description=site_group_1_name
)

site = nb.dcim.sites.create(          # all dependencies should be created prior to this
    name=site_name,
    slug=site_slug,
    description=site_desc,
    time_zone=site_time_zone,
    physical_address=site_physical_address,
    shipping_address=site_shipping_address,
    facility=site_facility,
    status=site_status,
    latitude=43.123456,
    longitude=-79.123456,
    group=site_group_1.id,
    site_group=site_group_1.id,
    region=local_region.id,           # this links the site to a region
    tenant=tenant.id,                    # links to tenant
    parent=local_region.id)              # make the local region the parent

site_location = nb.dcim.locations.create(
    name=location_1, slug=location_slug_1,
    latitude=43.123456,
    longitude=-79.123456,
    description=location_desc_1,
    address=location_address_1,
    site=site.id,
    tenant=tenant.id)

print(site, country_region, site_location, tenant)
