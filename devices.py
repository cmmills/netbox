#! /usr/bin/env python

from platform import platform
from pyexpat import model
import pynetbox

token = "0123456789abcdef0123456789abcdef01234567"
url = "http://10.0.0.134:8000"
dev1 = "pve-1"
dev2 = "pve-2"
dev3 = "pve-3"
dev4 = "pve-4"

# Create a NetBox object
nb = pynetbox.api(url, token=token)

# Define items needed for the device

manufacturer_1 = nb.dcim.manufacturers.create(
    name="Micro-Star International Co., Ltd",
    slug="msi",
    description="Micro-Star",
)

device_type_1 = nb.dcim.device_types.create(
    manufacturer=manufacturer_1.id,
    model="MS-7B33",
    slug="ms-7b33",
)

manufacturer_2 = nb.dcim.manufacturers.create(
    name="Dell Inc",
    slug="vostro",
    description="Dell Inc",
)

device_type_2 = nb.dcim.device_types.create(
    manufacturer=manufacturer_2.id,
    model="Vostro 430",
    slug="vostro-430",
    serial="FNW0PM1",
)

manufacturer_3 = nb.dcim.manufacturers.create(
    name="Intel(R) Client Systems",
    slug="NUC",
    description="Intel NUC",
)

device_type_3 = nb.dcim.device_types.create(
    manufacturer=manufacturer_3.id,
    slug="nuc713",
    model="Inel NUC",
)

device_type_4 = nb.dcim.device_types.create(
    manufacturer=manufacturer_3.id,
    slug="nuc515",
    model="Intel NUC",
)

device_role_1 = nb.dcim.device_roles.create(
    name="Proxmox",
    slug="virtual-server",
    color="ffff00",
    vm_role=False,
    description="Proxmox Virtual Server Node",
)

device_1 = nb.dcim.devices.create(
    name=dev1,
    device_type=device_type_1.id,
    device_role=device_role_1.id,
    site=1,
    tenant=1,
    location=1,
    # platform=5,
    status="active",
)

device_2 = nb.dcim.devices.create(
    name=dev2,
    device_type=device_type_3.id,
    device_role=device_role_1.id,
    site=1,
    tenant=1,
    location=1,
    # platform=5,
    status="active",
)

device_3 = nb.dcim.devices.create(
    name=dev3,
    device_type=device_type_3.id,
    device_role=device_role_1.id,
    site=1,
    tenant=1,
    location=1,
    # platform=5,
    status="active",
)

device_4 = nb.dcim.devices.create(
    name=dev4,
    device_type=device_type_2.id,
    device_role=device_role_1.id,
    site=1,
    tenant=1,
    location=1,
    # platform=5,
    status="active",
)

devices = nb.dcim.devices.get(name=dev1)
print(devices)
