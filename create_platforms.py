#! /usr/bin/env python

import pynetbox

token = '0123456789abcdef0123456789abcdef01234567'
url = 'http://10.0.0.134:8000'

# Establish connection to NetBox
nb = pynetbox.api(url, token=token)

# Create manufacturer if it doesn't exist
canonical = nb.dcim.manufacturers.create(
    name='Canonical',
    slug='canonical',
    description='Canonical Ltd.',
)

debian = nb.dcim.manufacturers.create(
    name='Debian',
    slug='debian',
    description='Debian',
)

plt = nb.dcim.platforms.create(
    name="Ubuntu-18.04",
    slug="ubuntu-18-04",
    manufacturer=canonical.id,
)

plt1 = nb.dcim.platforms.create(
    name="Ubuntu-20.04.4",
    slug="ubuntu-20-04",
    manufacturer=canonical.id,
)

plt2 = nb.dcim.platforms.create(
    name="Ubuntu-22.04",
    slug="ubuntu-22-04",
    manufacturer=canonical.id,
)

plt3 = nb.dcim.platforms.create(
    name="Ubuntu-22.10",
    slug="ubuntu-22-10",
    manufacturer=canonical.id,
)

plt4 = nb.dcim.platforms.create(
    name="debian-11",
    slug="debian-11",
    manufacturer=debian.id,
)
