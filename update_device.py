#! /usr/bin/env python3

import pynetbox

token = '0123456789abcdef0123456789abcdef01234567'
url = 'http://10.0.0.134:8000'
# Establish connection to NetBox
nb = pynetbox.api(url, token=token)

# Change device state
# States can be either: planned, active, offline, staged, failed, inventory, decommissioning, decommissioned
device_name = "pve-2"
new_state = "active"

device = nb.dcim.devices.get(name=device_name)
device_update_dict = dict(
    status=new_state
)
device.update(device_update_dict)
print(device, device_name, device.status)
