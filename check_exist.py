#! /usr/bin/env python3

import pynetbox

token = '0123456789abcdef0123456789abcdef01234567'
url = 'http://10.0.0.134:8000'
manufacturer_1 = 'Dell Inc'

nb = pynetbox.api(url, token=token)
for manufacturer in nb.dcim.manufacturers.all():
    if manufacturer.name == manufacturer_1:
        print("Manufacturer", manufacturer_1, "already exists")
        break
else:
    print("Manufacturer", manufacturer_1, "does not exist")
